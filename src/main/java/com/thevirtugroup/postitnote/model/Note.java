package com.thevirtugroup.postitnote.model;


public class Note {
    private int id;
    private String name;
    private String summary;

    public Note() {
    }

    public Note(Integer id, String name, String summary) {
        this.id = id;
        this.name = name;
        this.summary = summary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
}
