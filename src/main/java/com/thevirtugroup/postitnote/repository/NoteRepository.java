package com.thevirtugroup.postitnote.repository;

import com.thevirtugroup.postitnote.model.Note;

import java.util.List;

public interface NoteRepository {
    List<Note> getAll();

    boolean update(Note note);

    boolean save(Note note);

    boolean remove(Integer id);
}
