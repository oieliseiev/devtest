package com.thevirtugroup.postitnote.repository.impl;

import com.thevirtugroup.postitnote.model.Note;
import com.thevirtugroup.postitnote.repository.NoteRepository;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Repository
public class NoteRepositoryImpl implements NoteRepository {

    private List<Note> noteList;
    private final AtomicInteger idCounter = new AtomicInteger();

    @PostConstruct
    private void init() {
        Note note1 = new Note(idCounter.incrementAndGet(), "note1", "This is note 1");
        Note note2 = new Note(idCounter.incrementAndGet(), "note2", "This is note 2");
        Note note3 = new Note(idCounter.incrementAndGet(), "note3", "This is note 3");
        Note note4 = new Note(idCounter.incrementAndGet(), "note4", "This is note 4");
        noteList = new ArrayList<>();
        noteList.add(note1);
        noteList.add(note2);
        noteList.add(note3);
        noteList.add(note4);
    }

    @Override
    public synchronized List<Note> getAll() {
        return noteList;
    }

    @Override
    public synchronized boolean update(Note note) {
        Note noteToUpdate = noteList.stream().filter(n -> n.getId() == note.getId()).findAny().orElseThrow(() -> new IllegalArgumentException("No notes found by id [" + note.getId() + "]"));
        noteToUpdate.setName(note.getName());
        noteToUpdate.setSummary(note.getSummary());
        return true;
    }

    @Override
    public synchronized boolean save(Note note) {
        note.setId(idCounter.incrementAndGet());
        return noteList.add(note);
    }

    @Override
    public synchronized boolean remove(Integer id) {
        Note noteToRemove = noteList.stream().filter(n -> n.getId() == id).findAny().orElseThrow(() -> new IllegalArgumentException("No notes found by id [" + id + "]"));
        return noteList.remove(noteToRemove);
    }
}
