package com.thevirtugroup.postitnote.rest;

import com.thevirtugroup.postitnote.model.Note;
import com.thevirtugroup.postitnote.service.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 */
@RestController
@RequestMapping(path = "/api/note")
public class NoteController {

    @Autowired
    private NoteService noteService;

    @RequestMapping(path = "/list", method = RequestMethod.GET)
    public List<Note> getAllNotes() {
        return noteService.getAll();
    }

    @RequestMapping(path = "/update", method = RequestMethod.PUT)
    public boolean update(@RequestBody Note note) {
        return noteService.update(note);
    }

    @RequestMapping(path = "/save", method = RequestMethod.POST)
    public boolean save(@RequestBody Note note) {
        return noteService.save(note);
    }

    @RequestMapping(path = "/remove/{id}", method = RequestMethod.DELETE)
    public boolean delete(@PathVariable Integer id) {
        return noteService.remove(id);
    }
}
