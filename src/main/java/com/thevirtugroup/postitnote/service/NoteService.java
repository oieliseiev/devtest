package com.thevirtugroup.postitnote.service;

import com.thevirtugroup.postitnote.model.Note;

import java.util.List;

public interface NoteService {
    List<Note> getAll();

    boolean update(Note note);

    boolean save(Note note);

    boolean remove(Integer id);
}
