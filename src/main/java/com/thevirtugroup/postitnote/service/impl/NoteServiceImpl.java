package com.thevirtugroup.postitnote.service.impl;

import com.thevirtugroup.postitnote.model.Note;
import com.thevirtugroup.postitnote.repository.impl.NoteRepositoryImpl;
import com.thevirtugroup.postitnote.service.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NoteServiceImpl implements NoteService {

    @Autowired
    private NoteRepositoryImpl noteRepository;

    @Override
    public List<Note> getAll() {
        return noteRepository.getAll();
    }

    @Override
    public boolean update(Note note) {
        return noteRepository.update(note);
    }

    @Override
    public boolean save(Note note) {
        return noteRepository.save(note);
    }

    @Override
    public boolean remove(Integer id) {
        return noteRepository.remove(id);
    }
}
