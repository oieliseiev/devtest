var app = angular.module('notesApp', ['ngRoute', 'ngMaterial', 'ngCookies']);

app.config(['$locationProvider', '$routeProvider',
    function ($locationProvider, $routeProvider) {

        $routeProvider
            .when('/', {
                templateUrl: '/partials/notes-view.html',
                controller: 'notesController'
            })
            .when('/login', {
                templateUrl: '/partials/login.html',
                controller: 'loginController',
            })
            .otherwise('/');
    }
]);

app.run(['$http', '$rootScope', '$location', '$cookieStore', function ($http, $rootScope, $location, $cookieStore) {
    $rootScope.$on('$routeChangeStart', function (event) {
        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            // $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata;
        }
        // redirect to login page if not logged in
        if ($location.path() !== '/login' && !$rootScope.globals.currentUser) {
            console.log('DENY');
            event.preventDefault();
            $location.path('/login');
        }
    });
}]);


app.service('AuthService', ['$http', '$cookieStore', '$rootScope', function ($http, $cookieStore, $rootScope) {

    var service = {
        login: login,
        setCredentials: setCredentials,
        clearCredentials: clearCredentials
    };

    return service;

    function login(username, password) {
        return $http.post("api/login", {username: username, password: password}).then(function (user) {
            console.log(user);
            return user;
        })
    }

    function setCredentials(username, password) {
        var authdata = username + ':' + password; //Base64 encoding should be used for Authorization header.

        $rootScope.globals = {
            currentUser: {
                username: username,
                authdata: authdata
            }
        };

        // $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata;
        $cookieStore.put('globals', $rootScope.globals);
    }

    function clearCredentials() {
        $rootScope.globals = {};
        $cookieStore.remove('globals');
        // $http.defaults.headers.common.Authorization = 'Basic ';
    }
}]);

app.controller('loginController', function ($scope, AuthService, $location) {

    $scope.invalidCreds = false;
    $scope.login = function () {
        AuthService.login($scope.login.username, $scope.login.password).then(function (user) {
            console.log(user);
            AuthService.setCredentials($scope.username, $scope.password);
            $location.path("/");
        }, function (error) {
            console.log(error);
            $scope.invalidCreds = true;
        });
    };

    $scope.logout = function () {
        console.log("logout");
        AuthService.clearCredentials();
        $location.path('/login');
    };
});

app.service('NoteService', ['$http', '$q', function ($http, $q) {

    var service = {
        getAll: getAll,
        save: save,
        update: update,
        remove: remove
    };

    return service;

    function getAll() {
        var deferred = $q.defer();
        $http.get('api/note/list')
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Failed to get all notes ' + errResponse);
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function save(note) {
        var deferred = $q.defer();
        $http.post("api/note/save", note)
            .then(function (response) {
                    deferred.resolve(response.data);
                }, function (errResponse) {
                    console.log("Failed to save note: " + errResponse)
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function update(note) {
        var deferred = $q.defer();
        $http.put("api/note/update", note)
            .then(function (response) {
                    deferred.resolve(response.data);
                }, function (errResponse) {
                    console.log("Failed to update note: " + errResponse)
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function remove(id) {
        var deferred = $q.defer();
        $http.delete("api/note/remove/" + id)
            .then(function (response) {
                    deferred.resolve(response.data);
                }, function (errResponse) {
                    console.log("Failed to remove note: " + errResponse)
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }
}]);


app.controller('notesController', ['$scope', 'NoteService', function ($scope, NoteService) {
    var self = this;
    self.notes = [];
    self.note = {id: '', name: '', summary: ''};
    self.noteEror = '';
    self.isEditCreateView = false;
    self.edit = edit;
    self.remove = remove;
    self.viewNote = viewNote;
    self.createOrUpdate = createOrUpdate;
    self.newNoteView = newNoteView;
    self.reset = reset;

    getAllNotes();

    function getAllNotes() {
        self.noteEror = '';
        NoteService.getAll()
            .then(
                function (response) {
                    self.notes = response;
                },
                function (errResponse) {
                    self.noteEror = errResponse;
                }
            );
    }

    function createOrUpdate() {
        if (self.note.id) {
            updateNote();
        } else {
            createNote();
        }
    }

    function createNote() {
        console.log("Create new note: " + self.note);
        self.noteEror = '';
        NoteService.save(self.note)
            .then(
                getAllNotes,
                reset(),
                function (errResponse) {
                    self.noteEror = errResponse;
                }
            );
    }

    function updateNote() {
        console.log("Update note: " + self.note);
        self.noteEror = '';
        NoteService.update(self.note)
            .then(
                getAllNotes,
                reset(),
                function (errResponse) {
                    self.noteEror = errResponse;
                }
            );
    }

    function removeNote(id) {
        self.noteEror = '';
        NoteService.remove(id)
            .then(
                getAllNotes,
                function (errResponse) {
                    self.noteEror = errResponse;
                }
            );
    }

    function edit(id) {
        self.noteEror = '';
        console.log('id to be edited', id);
        for (var i = 0; i < self.notes.length; i++) {
            if (self.notes[i].id === id) {
                self.isEditCreateView = true;
                self.note = angular.copy(self.notes[i]);
                break;
            }
        }
    }

    function remove(id) {
        self.noteEror = '';
        console.log('id to be deleted', id);
        var r = confirm("Are you sure you want to delete this note?");
        if (r == true) {
            if (self.note.id === id) {
                reset();
            }
            removeNote(id);
        }
    }

    function reset() {
        self.noteEror = '';
        self.note = {id: '', name: '', summary: ''};
        self.isEditCreateView = false;
    }

    function newNoteView() {
        console.log('newNoteView');
        reset();
        self.isEditCreateView = true;
    }

    function viewNote(id) {
        self.noteEror = '';
        console.log('id to be edited', id);
        for (var i = 0; i < self.notes.length; i++) {
            if (self.notes[i].id === id) {
                self.isEditCreateView = false;
                self.note = angular.copy(self.notes[i]);
                break;
            }
        }
    }
}]);
